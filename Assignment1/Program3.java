// Print the following pattern                                                                                                                                        //   5                                                                                                                                                                //   6  8                                                                                                                                                             //   7  10  13                                                                                                                                                        //   8  12  16  20                                                                                                                                                    //   9  14  19  24  29  

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the row numbers:");
		int row =sc.nextInt();
		int num =4;
		for(int i=1;i<=row;i++){
			int temp =num;
			for(int j=1;j<=i;j++){
				temp+=i;
				System.out.print(temp+" ");
			}
			System.out.println();
		}
	}
}
