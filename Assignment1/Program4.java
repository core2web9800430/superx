// print the following pattern
//          1
//       2  4
//    3  6  9
// 4  8  12 16
//
import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the row numbers:");
		int row =sc.nextInt();
		int num =0;
		for(int i=1;i<=row;i++){
			int temp =num;
			for(int j=1;j<=row;j++){
				if(j>=row+1-i){
				temp+=i;
				System.out.print(temp+" ");
				 } else {
				 System.out.print("  ");
				 }	 
			}
			System.out.println();
		}
	}
}
