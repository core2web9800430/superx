// print the following pattern
//   0
//   3  8
//   15 24 35
//   48 63 80 99

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the row numbers:");
		int row =sc.nextInt();
		int num =1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(num*num-1+" ");
				num++;
			}
			System.out.println();
		}
	}
}
