//write a program to print Prime number in given range

import java.util.*;
class PrimeNumber {
	static boolean prime( int num ){
		int count =0;
		for(int i=1;i<=num;i++){
			if(num%i==0)
				count++;
		} 
		if (count==2)
			return true;
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the first Number");
		int n1 = sc.nextInt();
		System.out.println("Enter the Second Number");
		int n2 = sc.nextInt(); 
		System.out.print("prime number betn "+n1+" to "+n2+" is: ");
		for(int i=n1;i<=n2; i++){
			boolean check = prime(i);
			if(check)
				System.out.print(i+" ");
		} 
		System.out.println();
	}
}
				

