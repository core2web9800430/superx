// Print the following Pattern
//   1  2  3  4
//   4  5  6  7
//   6  7  8  9
//   7  8  9  10

import java.util.*;
class Pattern {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows:");
		int row = sc.nextInt();
		int num=1; int k=1;
		for (int i = 1; i <= row; i++) {
			for (int j = 1; j <= row; j++) {
				System.out.print(num + " ");
				num++;
			}
			System.out.println();
			num=num-k++;
		}
	}
}

