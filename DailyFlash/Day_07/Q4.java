// WAP to print the Perfect number in a given range.
// Input: 1 to 50
// Input: 40 to 100
//

import java.util.*;
class PerfectNumber {
	static boolean isPerfectNumber(int num){
		int sum=0;
		for(int i= 1;i<=num/2;i++){
			if(num%i==0)
				sum+=i;
		}
		if(sum == num)
			return true;
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Starting number:");
		int start = sc.nextInt();
		System.out.println("Enter the ending number:");
		int end= sc.nextInt();
		for(int i = start;i<=end;i++){
			 boolean check= isPerfectNumber(i);
			 if(check)
				 System.out.print(i+" ");
		}
	}
}

