/* WAP to print the following pattern
   Take row input from the user
   10
   9 8
   7 6 5
   4 3 2 1
*/

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of rows :");
		int row = sc.nextInt();
		int num =10;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
			System.out.print(num-- +" ");
			}
			System.out.println();
		}
	}
}

