// WAP to check whether the given number is Duck number or not.

import java.util.Scanner;

public class DuckNumberChecker {
	         static boolean isDuckNumber(String number) {
	                        if (number.charAt(0) == '0') {
	                                     return false;
				}
				for (int i = 1; i < number.length(); i++) {
					if (number.charAt(i) == '0') {
						return true;
					}
				}
				return false;
		 }
		 public static void main(String[] args) {
			 Scanner scanner = new Scanner(System.in);

			 System.out.print("Enter a number: ");
			 String input = scanner.next();

			 if (isDuckNumber(input)) {
	                                                                                                                                                                 System.out.println(input + " is a Duck number.");
			 } else {
				 System.out.println(input + " is not a Duck number.");
			 }
		 }
}
	    
