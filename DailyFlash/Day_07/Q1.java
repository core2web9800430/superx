/* WAP to print the following pattern
   Take input from the user
   1 2 3 4
   a b c d
   5 6 7 8
   e f g h
   9 10 11 12
*/

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of rows :");
		int row = sc.nextInt();
		int num =1;
		char ch = 'a';
		for(int i=1;i<=row;i++){
			for(int j=1;j<=4;j++){
				if(i%2==0)
					System.out.print(ch++ +" ");
				else
					System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}

