// WAP to check whether the given no is prime or composite

import java.util.*;
class PrimeNumber {
	static boolean isprime(int n){
		int count =0;
		if(n<=1)
			return false;
		for(int i=1;i*i<=n;i++){
			if(n%i==0) 
			count+=2;
		}
		if(count == 2){
			return true;
		}
		return false;
	}
	public static void main(String [] args ){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the number:");
		int n = sc.nextInt();
		boolean check= isprime(n);
		if(check)
			System.out.println("It is a prime Number");
		else 
			System.out.println("Not Prime Number");
	}
}

