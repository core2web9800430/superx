//WAP to print the composite numbers in the given range
//Input: start:1
//end:100

import java.util.*;
class CompositeNumber {
	static void compositeNum(int start , int end){
		for (int i=start;i<=end;i++){
		int count =0;
		for(int j=1;j*j<=i;j++){
			if(i%j==0) 
			count+=2;
		}
		if(count>2)
			System.out.print(i+" ");
		}
	}
	public static void main(String [] args ){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter start number:");
		int start = sc.nextInt();
		System.out.println("Enter the End number:");
		int end = sc.nextInt();
		compositeNum(start,end);
	}
}

