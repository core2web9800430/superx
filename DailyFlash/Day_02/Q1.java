/* WAP to print the following pattern
  Take input from user
  A B C D
  B C D E
  C D E F
  D E F G
*/

import java.util.*;
class Pattern {
	static void printPattern(int row){
		char ch = 'A';
		for(int i=1;i<=row;i++){
			char ch2=ch;
			for(int j=1;j<=row;j++){
				System.out.print(ch2++ +" ");
			}
			System.out.println();
			ch++;
		}
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Eneter the size of rows ");
		int row =sc.nextInt();
		printPattern(row);
	}
}


