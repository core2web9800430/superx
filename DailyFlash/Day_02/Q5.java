//WAP to check whether the string contains vowels and return the count of vowels.

import java.util.*;
class StringVowels {
	static void countOff( String str){
		char ch [] = str.toCharArray();
		int a=0,e=0, I=0, o=0,u=0;
		for(char c:ch){
			if(c=='a')
				a++;
			if(c=='e')
				e++;
			if(c=='i')
				I++;
			if(c=='o')
				o++;
			if(c=='u')
				u++;
		}
		System.out.println("count of vowels is :");
		System.out.println("a : "+a);
		System.out.println("e : "+e);
		System.out.println("i : "+I);
		System.out.println("o : "+o);
		System.out.println("u : "+u);
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string in smallcase only:");
		String str = sc.nextLine();
		countOff(str);
	}
}
