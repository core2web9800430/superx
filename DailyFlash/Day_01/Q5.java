//WAP to count the size of given string
//Note:(without using inbuilt method)

import java.util.*;
class CountString {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String ");
		String str = sc.nextLine();
		char ch [] = str.toCharArray();
		int count=0;
		for(char c:ch){
			count++;
		}
		System.out.println("Size of String is: "+count);
	}
}


