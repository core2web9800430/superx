/*WAP to print the following pattern Take input from user
   
  1 2 3 4
  2 3 4 5
  3 4 5 6
  4 5 6 7
*/
    
import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Eneter the size of row:");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=i;
			for(int j=1; j<=row; j++){
				System.out.print(num++ +" ");	
			}
			System.out.println();
		}
	}
}


