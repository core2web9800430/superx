/* WAP to print the odd numbers in the given range
Input: start:1
end:10
*/
import java.util.*;
class EvenOdd {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Eneter the starting number:");
		int start = sc.nextInt();
		System.out.println("Eneter the ending number:");
		int end = sc.nextInt();
		System.out.println("Even Numbers:");
		for(int i=start;i<=end;i++){
			if(i%2==0)
				System.out.print(i+" ");
		}
		System.out.println();
		System.out.println("Odd Numbers:");
			for(int i=start; i<=end; i++){
				if(i%2!=0)
				System.out.print(i +" ");	
			}
			System.out.println();
	}
}


