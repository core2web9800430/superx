/* Given a sorted array of distinct integers and a target value, return the index if the target
is found. If not, return the index where it would be if it were inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4
Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
*/
import java.util.*;
class SearchInsertPosition {
 static int position(int [] arr,int target){
	 int start=0;
	 int end = arr.length-1;
	 while(start<=end){
		 int mid = (start+end)/2;
		 if(arr[mid]==target)
			 return mid;
		 else if(target>arr[mid])
			 start=mid+1;
		 else
			 end=mid-1;
	 }
	 return end+1;
 }
 public static void main(String[] args){
	 Scanner sc = new Scanner(System.in);
	 System.out.println("Enter the size of an Array:");
	 int n= sc.nextInt();
	 int [] arr= new int[n];
	 System.out.println("Enter the elements in array:");
	 for(int i= 0;i<n;i++){
		 arr[i]=sc.nextInt();
	 } 
	 System.out.println("Enter the target value:");
	 int target= sc.nextInt();
	 int ret = position(arr,target);
	 System.out.println(ret);
 }
}


