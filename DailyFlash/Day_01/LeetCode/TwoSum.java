/* Given an array of integers nums and an integer target, return indices of the two
numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use
the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
*/

import java.util.*;
class TwoSum {
	static void index(int arr [],int target ){
		int start =0; 
		int end =0;
		for(int i=0; i<arr.length;i++){
		    int	sum=0;
			for(int j=i;j<arr.length;j++){
				sum=sum+arr[j];
				if(sum==target){
					start=i;
					end=j;
				}
	        	} 
		}
		System.out.println(start+" "+ end);  
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n= sc.nextInt();
		int [] arr=new int[n];
		System.out.println("Enter the Elements in array:");
		for(int i=0;i<n;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the target Element:");
		int target= sc.nextInt();
		index(arr,target);
	}
}



