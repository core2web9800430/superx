/* WAP to print perfect numbers in a given range.
   Input: 1 to 10
*/

import java.util.*;
class PerfectNumber {
	static boolean perfectNumber(int num){
		int totalsum=0;
		for(int i=1;i<=num/2;i++){
			if(num%i==0) {
				totalsum+=i;
			}
		}
		if(totalsum == num)
			return true;
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Starting Number");
		int start = sc.nextInt();
		System.out.println("Enter Ending Number");
		int end = sc.nextInt();
		for(int i= start;i<=end;i++){
		boolean check= perfectNumber(i);
		if(check)
		   System.out.print(i+" ");
		}
	}
}
