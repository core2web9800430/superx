//WAP to check whether the given number is perfect or not.

import java.util.*;
class PerfectNumber {
	static boolean perfectNumber(int num){
		int totalsum=0;
		for(int i=1;i<=num/2;i++){
			if(num%i==0) {
				totalsum+=i;
			}
		}
		if(totalsum == num)
			return true;
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		int num = sc.nextInt();
		boolean check= perfectNumber(num);
		if(check)
			System.out.println(num+" It is a perfect Number");
		else
			System.out.println("No! -not perfect Number");
	}
}
