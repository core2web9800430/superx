/* WAP to print the following pattern
   Take row input from the user
    1
    7 26
    63 124 215
    342 511 728 999
*/

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of rows:");
		int row = sc.nextInt();
		int num =2;
		System.out.println("1");
		for( int i=2;i<=row;i++){
			for(int j=1; j<=i;j++){
				System.out.print(num*num*num-1+" ");
				num++;
			}
		System.out.println();
		}
	}
}
			
