/* WAP to print the following pattern
   Take row input from the user
   A
   B A
   C B A
   D C B A
*/

import java.util.*;
class Pattern { 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of rows ");
		int row = sc.nextInt();
		char ch = 'A';
		for(int i=1; i<=row ; i++){
			char ch2 = ch;
			for(int j=1;j<=i; j++){
			System.out.print(ch2-- +" ");
			}
			System.out.println();
			ch++;
		}
	}
}
