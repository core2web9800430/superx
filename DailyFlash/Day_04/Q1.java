/* WAP to print the following pattern
   Take input from the user
   A B C D
   # # # #
   A B C D
   # # # #
   A B C D
*/

import java.util.*;
class Pattern { 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of rows ");
		int row = sc.nextInt();
		for(int i=1; i<=row ; i++){
			char ch = 'A';
			for(int j=1;j<=4; j++){
				if(i%2==0)
					System.out.print("#"+" ");
				else
					System.out.print(ch++ +" ");
			}
			System.out.println();
		}
	}
}
