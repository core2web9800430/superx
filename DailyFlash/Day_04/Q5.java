/*  WAP to toggle the String to uppercase or lowercase
    Input: Java output: jAVA
    Input: data output: DATA
*/

import java.util.*;
class StringDemo {
     static String toggle( String str){
	     String str2="";
	     for(int i=0;i<str.length();i++){
		     if(Character.isUpperCase(str.charAt(i))){
			     str2+=Character.toLowerCase(str.charAt(i));
		     } else {
			     str2+=Character.toUpperCase(str.charAt(i));
		     }
	     } 
	     return str2;
     }
     public static void main(String [] args){
	     Scanner sc = new Scanner(System.in);
	     System.out.println("Enter the String :");
	     String str=sc.nextLine();
	     String str2=toggle(str);
	     System.out.println(str2);
     }
}


