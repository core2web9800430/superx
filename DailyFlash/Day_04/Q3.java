// Que 3: WAP to find the factorial of a given number.

import java.util.*;
class Factorial { 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number: ");
		int num = sc.nextInt();
		int fact=1;
		while(num>0){
			fact = fact*num--;
		}
		System.out.println("factorial is: "+fact);
	}
}
