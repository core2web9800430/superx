/* WAP to print the sum of digits in a given range.
   Input: 1 to 10
   Input: 21 to 30
*/

import java.util.*;
class RangeSum { 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter starting Digit: ");
		int start = sc.nextInt();
		System.out.println("Enter Endiging Digit: ");
		int end = sc.nextInt();
		int sum=0;
		for(int i=start; i<=end ; i++){
			sum+=i;
		}
			System.out.println(" Sum from "+start+" to "+end+" is: "+sum);
	}
}
