/*Multiply Strings (LeetCode - 43)
	Given two non-negative integers num1 and num2 represented as strings,
	return the product of num1 and num2, also represented as a string.
	Note: You must not use any built-in BigInteger library or convert the inputs to integers directly.
	Example 1:
	Input: num1 = "2", num2 = "3"
	Output: "6"
	Example 2:
	Input: num1 = "123", num2 = "456"
	Output: "56088"
	Constraints:
	1 <= num1.length, num2.length <= 200
	num1 and num2 consist of digits only.
	Both num1 and num2 do not contain any leading zero, except the number 0 itself.
*/ 
import java.util.*;
class MultiplyString {
	static String pravin(String num1,String num2){
		int n1= Integer.parseInt(num1);
		int n2 = Integer.parseInt(num2);
	        int temp =  n1*n2;
	        String s=String.valueOf(temp);
	        return s;	
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first number");
		String num1=sc.nextLine();
		System.out.println("Enter second number");
		String num2=sc.nextLine();
		String str = pravin(num1,num2);
		System.out.println(str);
	}
}
                

