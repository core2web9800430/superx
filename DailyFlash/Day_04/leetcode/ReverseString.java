/*Write a function that reverses a string. The input string is given as an array of characters s.
You must do this by modifying the input array in-place with O(1) extra memory.
Example 1:
Input: s = ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
Example 2:
Input: s = ["H","a","n","n","a","h"]
Output: ["h","a","n","n","a","H"]
Constraints:
1 <= s.length <= 10^5  s[i] is a printable ascii character.*/

import java.util.*;
class StrimgRevertse{

	static char[] Reverse(char arr[]){
			
		int length= arr.length;
		for(int i=0;i<length;i++){

			char temp = arr[i];
		arr[i]=arr[length-1];
		arr[length-1]=temp;
		length--;
		}
		return arr;
	}

		public static void main(String[] args){

			Scanner sc= new Scanner(System.in);
	
			System.out.println("Enter Size of Array");

			
			int size=sc.nextInt();
			char arr[]=new char[size];
			System.out.println("Enter Array Elements");
			for(int i=0;i<size;i++){

				arr[i]=sc.next().charAt(0);
			}

		char revarr[]=Reverse(arr);
		
		System.out.println("Reverse Array will Be:");
		for(int i=0;i<size;i++){

			System.out.print(revarr[i]+"  ");
		}
		}
}





