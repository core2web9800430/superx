/*WAP to print the factorial of digits in a given range.
Input: 1-10
*/

import java.util.Scanner;

class Q1 {
    static int factorial(int num){
        int fact=1;
        while(num>0){
            fact*=num;
            num--;
        }
        return fact;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter starting number:");
        int start = sc.nextInt();
        System.out.println("Enter the ending number:");
        int end = sc.nextInt();
        System.out.println("factorial of digits in given range is:");
        for (int i = start; i <=end; i++) {
            int fact=factorial(i);
            System.out.print(fact+" ");
        }
        
    }
}