/* WAP to print the following pattern
Take row input from the user
a
A B
a b c
A B C D
*/
import java.util.*;
public class Q2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the no of rows");
        int row = sc.nextInt();
        for (int i = 1; i<= row; i++) {
            char ch='A';
            char ch2='a';
            for (int j = 1; j <=i; j++) {
                if(i%2!=0)
                System.out.print(ch2++ +" ");
                else
                System.out.print(ch++ +" ");                
            }
            System.out.println();
        }
    }
}
