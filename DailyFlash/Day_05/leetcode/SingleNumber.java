/* Single Number (LeetCode-136)
	Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
	You must implement a solution with a linear runtime complexity and use only constant extra space.
	Example 1:
	Input: nums = [2,2,1]
	Output: 1
	Example 2:
	Input: nums = [4,1,2,1,2]
	Output: 4
	Example 3:
	Input: nums = [1]
	Output: 1
	Constraints:
	1 <= nums.length <= 3 * 104
	-3 * 104 <= nums[i] <= 3 * 104
	Each element in the array appears twice except for one element which appears only once.
*/

   import java.util.*;
   class SingleNumber {
	   static int singleNumber(int arr[]){
		   for(int i=0;i<arr.length;i++){
			   int count=0;
			   for(int j=0;j<arr.length; j++){
				   if(arr[i]==arr[j])
					   count++;
			   } 
			   if(count!=2)
			   return arr[i];
		   }
			   return -1;
	   }
	   public static void main(String [] args){
		   Scanner sc = new Scanner(System.in);
		   System.out.println("Enter the size of array:");
		   int n = sc.nextInt();
		   int arr [] = new int[n];
		   System.out.println("Enter the Elements in array:");
		   for(int i=0; i<n; i++){
			   arr[i]= sc.nextInt();
		   }
		   int ret = singleNumber(arr);
		   System.out.println(" Single Elements is: "+ret);
	   }
   }
