/* WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3
*/

import java.util.Scanner;

public class Q5 {
    static int countOf(String str, char ch){
        char arr[]=str.toCharArray();
        int count =0;
        for(char c:arr){
            if(c==ch)
            count++;
        }
        return count;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the String:");
        String str= sc.nextLine();
        System.out.println("Enter the character to be search:");
        char ch = sc.next().charAt(0);
        int count=0;
        count = countOf(str, ch);
        if(count>0)
        System.out.println(ch+" is present "+ count + " times present in string");
        else
        System.out.println(ch+" is not present in string ");
    }
}
