// WAP to check whether the given number is a strong number or not.

import java.util.Scanner;

public class Q3 {
    static int factorial(int num){
        int fact=1;
        while(num>0){
            fact*=num;
            num--;
        }
        return fact;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number :");
        int n= sc.nextInt();
        int num=n;
        int sum = 0;
        while(num>0){
            int digit=num%10;
            int fact=factorial(digit);
            sum+=fact;
            num/=10;
        }
        if(n==sum)
        System.out.println(n+" is a strong number");
        else
        System.out.println(n+" is not strong number");
    }
}
