/* WAP to find repeating character in the given string.
   Input → str : pneumonoultramicroscopicsilicovolcanoconiosis
*/
import java.util.*;
class StringDemo {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String");
		String str= sc.nextLine();
		int count=0;
		char [] ch = str.toCharArray();
		for(int i =0; i<ch.length;i++){
			for(int j=i;j<ch.length;j++){
				if(ch[i]==ch[j])
					count++;
			} 
			if(count>0)
				System.out.print(ch[i]+" ");
		}
	}
}


