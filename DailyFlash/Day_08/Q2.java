/* WAP to print the following pattern
   Take row input from the user
    4 3 2 1
    C B A
    2 1
    A
*/

import java.util.*;
class Pattern  {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter the no of rows:");
		int row = sc.nextInt();
		int num =5;
		char ch = '@';
		for( int i =1; i<=row;i++){
			for( int j =i; j<=row;j++){
				if(i%2==0){
					System.out.print(--ch+" ");
					++num;
				} else{
					System.out.print(--num+" ");
					++ch;
				}
			} 
		if(i%2==0)
			ch--;
		else
			num--;
		System.out.println();
		}
	}
}
			

