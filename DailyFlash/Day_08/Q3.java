// WAP to check whether the given number is an Automorphic Number or not.
//a number whose square has the same digits in the end as the number itself\
//

import java.util.*;
class Automorphic {
	static boolean automorphic(int num){
		int temp = num%10;
		int mult=(num*num)%10;
		if(temp==mult)
			return true;
		return false;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();
		boolean check = automorphic(num);
		if(check)
			System.out.println("It is Automorphic Number");
		else
			System.out.println("Not Automorphic Number");
	}
}

	
