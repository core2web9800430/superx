/*: WAP to print the following pattern
    Take input from the user
      D C B A
      5 4 3 2
      F E D C
      7 6 5 4
*/

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no of rows:");
		int row = sc.nextInt();
		int num=0;
		char ch  = 'D';
		for(int i=1; i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==0){
					System.out.print(num +" ");
					num-=1;
			                ch++;
				}
				else{
					System.out.print(ch-- +" ");
			             num++;
				}
			} 
			ch++;
			num++;
			System.out.println();
		}
	}
}

