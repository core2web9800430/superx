/* WAP to print each reverse numbers in the given range
   Input: start:25435
   end: 25449
*/

import java.util.*;
class Palindrome {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Start number of range:");
		int start = sc.nextInt();
                System.out.println("Enter the End number of range:");
		int end = sc.nextInt();
		for(int i=start;i<=end;i++){
		int num =i;
		int sum=0;
		while(num!=0){
			int temp = num%10;
			sum=sum*10+temp;
			num=num/10;
		} 
			System.out.print(sum +" ");
		}
	}
}

