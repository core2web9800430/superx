/* WAP to print the following pattern
   Take row input from user
   1
   2 1
   3 2 1
   4 3 2 1
*/

import java.util.*;
class Pattern {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the rows:");
		int row = sc.nextInt();
		for(int i=1;i<=row;i++){ 
			int num =i;
	        for(int j=1;j<=i;j++){
                	System.out.print(num-- +" ");
		} 
		System.out.println();
		}
	}
}

