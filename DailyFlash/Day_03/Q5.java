// WAP to check whether the string contains characters other than letters.

import java.util.*;
class Palindrome {
       	static boolean stringNonLetters(String str) {
		        for (char c : str.toCharArray()) {
				if (!Character.isLetter(c)) {
					return true;
				}
			}
			return false;
	}

	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String:");
		String str = sc.nextLine();
		boolean check = stringNonLetters(str);
		if(check)
			System.out.println("contain characters/number");
		else
			System.out.println("not contain characters/number");
	}
}

