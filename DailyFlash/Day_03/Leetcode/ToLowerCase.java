/* To Lower Case (Leetcode-709)
   Given a string s, return the string after replacing every uppercase letter with the same lowercase letter.
	Example 1:
	Input: s = "Hello"
	Output: "hello"
	Example 2:
	Input: s = "here"
	Output: "here"
	Example 3:
	Input: s = "LOVELY"
	Output: "lovely"
	Constraints:
	1 <= s.length <= 100
	s consists of printable ASCII characters
*/

import java.util.*;
class StringDemo {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String");
		String str= sc.nextLine();
		str =str.toLowerCase();
		System.out.println(str);
	}
}
