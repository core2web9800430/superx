/*Plus One (Leetcode-66)
   You are given a large integer represented as an integer array digits, 
   where each digits[i] is the ith digit of the integer. The digits are
   ordered from most significant to least significant in left-to-right order.
   The large integer does not contain any leading 0's.
   Increment the large integer by one and return the resulting array of digits.
	Example 1: Input: digits = [1,2,3]
	           Output: [1,2,4]
	Explanation: The array represents the integer 123.Incrementing by one gives 123 + 1 = 124. Thus, the result should be [1,2,4].
	Example 2: Input: digits = [4,3,2,1]
	           Output: [4,3,2,2]
	Explanation: The array represents the integer 4321. Incrementing by one gives 4321 + 1 = 4322. Thus, the result should be [4,3,2,2].
	Example 3: Input: digits = [9]
	           Output: [1,0]
	Explanation: The array represents the integer 9. Incrementing by one gives 9 + 1 = 10. Thus, the result should be [1,0].
	Constraints:
	1 <= digits.length <= 100
	0 <= digits[i] <= 9 digits does not contain any leading 0's.
*/

import java.io.*;
class ArrayDemo {
          static int [] plusOne(int [] arr){
		  for(int i=arr.length-1;i>=0;i--){
			  if(arr[i]<9){
				  arr[i]++;
				  return arr;
			  } else {
				  arr[i]=0;
			  }
		  } 
			  int arr2[] = new int [arr.length+1];
			  arr2[0]=1;
                           return arr2;
		  }


	public static void main(String []args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                
		System.out.println("Enter array size");
                 int N= Integer.parseInt(br.readLine());
                 int arr[] =new int[N];
		System.out.println("Enter array Element");
	          for(int x=0;x<N;x++){
		     arr[x] = Integer.parseInt(br.readLine());	  
		  } 
		   arr = plusOne(arr);
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}	
	}
}
