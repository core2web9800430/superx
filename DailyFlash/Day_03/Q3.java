// Que 3 : WAP to check whether the given no is a palindrome number or not.

import java.util.*;
class Palindrome {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number:");
		int n = sc.nextInt();
		int sum=0, num=n;
		if(n<=0){
			System.out.println(n+" is Not Palindrome Number");
		} else{
		while(num!=0){
			int temp = num%10;
			sum=sum*10+temp;
			num=num/10;
		} 
		if(sum==n)
			System.out.println(n+" is Palindrome Number");
		else 
			System.out.println(n+" is Not Palindrome Number");
		}
	}
}

