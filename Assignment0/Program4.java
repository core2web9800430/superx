// write a program to print following pattern
// A b C d E
// e D c B
// B c D
// d C
// C
import java.util.*;
class PatternDemo {
	public static void main(String[] args){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the size of row ");
	int rows=sc.nextInt();
	char ch = 'A';
	int val = (int)('a'-'A');
	for(int i = 1 ; i<=rows ; i++) {
		for(int j = 1 ; j <= rows-i+1 ; j++) {
			if(i%2 == 0) { 
				if(j%2!=0) {
					System.out.print((char)(ch+val)+" ");
				}else {
					System.out.print(ch + " ");
				}
				if(j<rows-i+1)
					ch--;
			}else {
				if(j%2 != 0) {
					System.out.print(ch + " ");
				}else {
					System.out.print((char)(ch+val) + " "); 
				} 
				if(j<rows-i+1)
					ch++;
			}
		}
		System.out.println(); 
	}
	}
}																																																																																																														
