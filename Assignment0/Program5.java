// write a program  to revers a number and put sucesive number sum into an array and print it 
// Input : 45689
// Output: 17,14,11,9,4
//

import java.util.*;
class Reverse {
	static void reverse(int num){
		int sum = 0;
		int temp1 = num;
		int count = 0;
		while(temp1!=0){
			count++;
			temp1 = temp1/10;
		}
		int arr[] = new int[count];
		int j = 0;
		while(num!=0){
			int rem = num%10;
			arr[j] = rem;
			num = num/10;
			j++;
		}
		for(int i=0;i<arr.length-1;i++){
			arr[i] = arr[i] + arr[i+1];
			System.out.print(arr[i]+", ");
		}
		System.out.print(arr[arr.length-1]);
		System.out.println();
	}
	public static void main(String[]args) {	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();
		reverse(num);
	}
}
