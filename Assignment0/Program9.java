/*write a program to take string from user and convert all even indexes of string to Uppercases and odd index of a string to lowercase
  Input : dfTbnSrOvryt
  Output: DfTbNsRoVrYt
 */

import java.util.*;
class StringDemo {
	public static void main(String[]args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String:");
		String str = sc.next();
		char ch[] = str.toCharArray();
		for(int i=0; i<ch.length; i++){														
			if(i%2==0) {
				System.out.print(Character.toUpperCase(ch[i]));
			}
			else{
				System.out.print(Character.toLowerCase(ch[i]));
			}
		}
		System.out.println();
	}
}
