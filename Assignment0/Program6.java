// write a program to find a number which has number on its left is less than or equal to itself
//  Input : 456975962
//  Output: 9

import java.util.*;
class ArrayDemo {
	static void fun(int n){
		int temp=n;
		int count=0;
		while(temp>0){
			count++;
			temp=temp/10;
		}
		int arr[]=new int[count];
		int j=0;
		while(n>0){
			int a=n%10;
			arr[j]=a;
			j++;
			n=n/10;
		}
		int min=arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i-1]>arr[i]){
				int t=arr[i];
				arr[i]=arr[i-1];
				arr[i-1]=temp;
			}
		}
		System.out.println(arr[arr.length-1]);
	} 
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number:");
		int n=sc.nextInt();
		fun(n);
	}
}

