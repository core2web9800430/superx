// write a program to print a given number is Armstrong number or not 
// Input : 153
// Output: it is an Armstrong Number 

import java.util.*;
class Armstrong {
	static void armstrong(int num){
		int num2=num;
		int temp=num;
		int sum=0;
		int count=0;
		while(temp!=0){
			count++;
			temp=temp/10;
		} 
		while (num2!=0){
			int digit = num2%10;
			sum=sum+(int)Math.pow(digit , count);
			num2=num2/10;
		}
		if(sum==num){
			System.out.println("It is an Armstrong Number ");
		} else{
			System.out.println(" Not an Armstrong Number");
		}
	}
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the Number ");
		int num=sc.nextInt();
		armstrong(num);
	}
}
