/*Write a program to find the Occurance of vowels in a given String
Input : adgtioseobi
Output: a = 1
        e = 1
        i = 2 
        o = 1
        u = 0
*/

import java.util.*;
class StringDemo {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string :");
		String str = sc.next();
		char[] arr = str.toCharArray();
		int a=0, e=0, I=0, o=0, u=0;
		for(int i=0; i<arr.length; i++) {
			if(arr[i] == 'a') {
				a++;
			} else if(arr[i] == 'e') {
				e++;
			} else if(arr[i] == 'i') {
				I++;
			}else if(arr[i] == 'o') {
				o++;	
			}else if(arr[i] == 'u') {
				u++;
			}
		}
		if(a!=0) {
			System.out.println("a = " + a);
		}
																			
		if(e!=0) {																				System.out.println("e = " + e);
		}
		if(I!=0) {
			System.out.println("i = " + I);
		}
		if(o!=0) {
			System.out.println("o = " + o);
		}
		if(u!=0) {
			System.out.println("u = " + u);
		}													
	}
}

