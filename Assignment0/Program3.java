//print the following pattern
//	D	C	B	A
// 	e	f	g	h
//  	F	E	D	C
//   	g       h	i	j
//  
 import java.util.*;
 class Pattern1{
	 public static void main(String[] args){
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter Rows");
		 int rows=sc.nextInt();
		 int ch;
		 for(int i=0;i<rows;i++){
			 ch=64+rows+i;
			 for(int j=0;j<rows;j++){
				 if(i%2==0){
					 System.out.print((char)ch+" ");
					 ch--;
				 }else{
					 System.out.print((char)(ch+32) +" ");
					 ch++;
				 }
			 }
			 System.out.println();
		 }
	 }
 }
