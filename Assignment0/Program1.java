// Write a program to print  the factorial of even number in a given number

import java.util.*;
class Factorial {
	static void factorial(int num){
		int fact=1;
		for(int i=1;i<=num;i++){
			fact *=i;
		}
		System.out.print(fact+" ");
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number :");
		int num= sc.nextInt();
		while(num!=0){
			int temp=num%10;
			if(temp%2==0){
				factorial(temp);
			}
			num=num/10;
		}
	}
}

